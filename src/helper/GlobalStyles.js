import {StyleSheet} from 'react-native';
import APP_COLORS from './Color';
const GLOBAL_STYLES = StyleSheet.create({
  CONTAINER: {
    flex: 1,
    backgroundColor: APP_COLORS.PRIMARY_COLOR,
  },
});
export default GLOBAL_STYLES;
