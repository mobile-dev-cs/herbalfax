const APP_COLORS = {
  PRIMARY_COLOR: '#fff667',
  BLACK: '#000000',
  WHITE: '#ffffff',
};

export default APP_COLORS;
