const INITIAL_STATE = {
  isLoading: false,
  error: undefined,
  data: undefined,
};

export default function DemoReducer(state = INITIAL_STATE, action) {
  //console.log(action.type);
  switch (action.type) {
    case 'DEMO_INIT':
      return {
        ...state,
        isLoading: true,
      };

    case 'DEMO_SUCCESS':
      return {
        ...state,
        isLoading: false,
        data: action.data,
        error: undefined,
      };
    case 'DEMO_FAILED':
      return {
        ...state,
        isLoading: false,
        error: action.error,
      };
    default:
      return state;
  }
}
